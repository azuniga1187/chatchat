# Marklogic NoSQL servers

resource "aws_instance" "marklogicnosql-z1-01" {
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_1
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z1-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z1-db.id

  vpc_security_group_ids = [
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.all-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z1-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

# marklogic-z2-01, conditionally created.
resource "aws_instance" "marklogicnosql-z2-01" {
  count = "${var.env == "dev" ? 0:1}"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_2
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z2-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z2-db.id

  vpc_security_group_ids = [
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.all-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z2-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

# marklogic-z3-01, conditionally created.
resource "aws_instance" "marklogicnosql-z3-01" {
  count = "${var.env == "dev" ? 0:1}"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_3
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z3-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z3-db.id

  vpc_security_group_ids = [
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.all-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z3-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

resource "aws_instance" "marklogicnosql-z1-02" {
  count = "${var.env == "prod" ? 1:0}"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_1
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true


  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z1-02-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z1-db.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z1-02_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

resource "aws_instance" "marklogicnosql-z2-02" {
  count = "${var.env == "prod" ? 1:0}"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_2
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z2-02-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z2-db.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z2-02_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

resource "aws_instance" "marklogicnosql-z3-02" {
  count = "${var.env == "prod" ? 1:0}"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_3
  instance_type     = var.ml_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnosql-z3-02-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z3-db.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.marklogic-app-sg.id,
    aws_security_group.nifi-sg.id,
  ]
  tags = {
    Name = var.marklogicnosql-z3-02_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}


# resource "aws_instance" "marklogicnosql-z3-03" {
#   count = "${var.env == "dev" ? 0:1}"
#   ami               = var.linux_default_ami
#   availability_zone = var.availability_zone_3
#   instance_type     = var.ml_instance_type[var.env]
#   key_name          = var.aws_key_name

#   monitoring = true

#   root_block_device {
#     volume_type = "gp2"
#     volume_size = "100"
#     encrypted   = var.ebs_encryption[var.env]
#     kms_key_id  = var.kms_key_ebs[var.env]
#     delete_on_termination = "false"
#   }

#   private_ip    = var.marklogicnosql-z3-03-private-ip[var.env]
#   subnet_id     = aws_subnet.subnet-z3-db.id

#   vpc_security_group_ids = [
#     aws_security_group.all-sg.id,
#     aws_security_group.marklogic-app-sg.id,
#     aws_security_group.nifi-sg.id,
#   ]
#   tags = {
#     Name = "${var.env}-${var.marklogicnosql-z3-03_name}"
#     LambdaBackupConfiguration = var.snapshot-retention-parameter
#   }
# }

# resource "aws_instance" "marklogicnosql-z1-03" {
#   ami               = var.linux_default_ami
#   availability_zone = var.availability_zone_1
#   instance_type     = var.ml_instance_type[var.env]
#   key_name          = var.aws_key_name

#   monitoring = true

#   root_block_device {
#     volume_type           = "gp2"
#     volume_size           = "100"
#     delete_on_termination = "false"
#     encrypted   = var.ebs_encryption[var.env]
#     kms_key_id  = var.kms_key_ebs[var.env]
#   }

#   private_ip    = var.marklogicnosql-z1-03-private-ip[var.env]
#   subnet_id     = aws_subnet.subnet-z1-db.id

#   vpc_security_group_ids = [
#     aws_security_group.all-sg.id,
#     aws_security_group.marklogic-app-sg.id,
#     aws_security_group.nifi-sg.id,
#   ]
#   tags = {
#     Name = "${var.env}-${var.marklogicnosql-z1-03_name}"
#     LambdaBackupConfiguration = var.snapshot-retention-parameter
#   }
# }


# resource "aws_instance" "marklogicnosql-z2-03" {
#   count		    = "${var.env == "dev" ? 0 : 1}"
#   ami               = var.linux_default_ami
#   availability_zone = var.availability_zone_2
#   instance_type     = var.ml_instance_type[var.env]
#   key_name          = var.aws_key_name

#   monitoring = true

#   root_block_device {
#     volume_type           = "gp2"
#     volume_size           = "100"
#     delete_on_termination = "false"
#     encrypted   = var.ebs_encryption[var.env]
#     kms_key_id  = var.kms_key_ebs[var.env]
#   }

#   private_ip    = var.marklogicnosql-z2-03-private-ip[var.env]
#   subnet_id     = aws_subnet.subnet-z2-db.id

#   vpc_security_group_ids = [
#     aws_security_group.all-sg.id,
#     aws_security_group.marklogic-app-sg.id,
#     aws_security_group.nifi-sg.id,
#   ]
#   tags = {
#     Name = "${var.env}-${var.marklogicnosql-z2-03_name}"
#     LambdaBackupConfiguration = var.snapshot-retention-parameter
#   }
# }

#marklogic apache nifi server, z1 01

resource "aws_instance" "marklogicnifi-z1-01" {
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_1
  instance_type     = var.nifi_instance_type[var.env]
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnifi-z1-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z1-app.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.nifi-8080-sg.id,
    aws_security_group.nifi-sg.id,
    aws_security_group.marklogic-app-sg.id,
  ]
  tags = {
    Name = var.marklogicnifi-z1-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

#nifi in zone 2, conditionally created.
resource "aws_instance" "marklogicnifi-z2-01" {
  count		= "${var.env == "dev" ? 0 : 1 }"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_2
  instance_type     = var.nifi_instance_type[var.env] 
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "100" 
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  ebs_optimized = false #tbd:  determine if should be true
  private_ip    = var.marklogicnifi-z2-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z2-app.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.nifi-8080-sg.id,
    aws_security_group.nifi-sg.id,
    aws_security_group.marklogic-app-sg.id,
  ]
  tags = {
    Name = var.marklogicnifi-z2-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}

#nifi in zone 3, conditionally created.
resource "aws_instance" "marklogicnifi-z3-01" {
  count		= "${var.env == "dev" ? 0 : 1 }"
  ami               = var.linux_default_ami
  availability_zone = var.availability_zone_3
  instance_type     = var.nifi_instance_type[var.env] 
  key_name          = var.aws_key_name
  iam_instance_profile = aws_iam_instance_profile.ec2-cloudwatchagent-profile.name

  monitoring = true

  ### Terraform ebs optimized bug workaround https://github.com/terraform-providers/terraform-provider-aws/issues/11878
  lifecycle {
    ignore_changes = [ebs_optimized]
  }
  
  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
    encrypted   = var.ebs_encryption[var.env]
    kms_key_id  = var.kms_key_ebs[var.env]
    delete_on_termination = "false"
  }

  private_ip    = var.marklogicnifi-z3-01-private-ip[var.env]
  subnet_id     = aws_subnet.subnet-z3-app.id

  vpc_security_group_ids = [
    aws_security_group.all-sg.id,
    aws_security_group.nifi-8080-sg.id,
    aws_security_group.nifi-sg.id,
    aws_security_group.marklogic-app-sg.id,
  ]
  tags = {
    Name = var.marklogicnifi-z3-01_name[var.env]
    LambdaBackupConfiguration = var.snapshot-retention-parameter
  }
}
