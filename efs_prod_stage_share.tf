resource "aws_efs_file_system" "tdm_catalyst_ml_prod_stage_share" {
  creation_token = var.ml-efs-token[var.env]
  encrypted      = var.ebs_encryption[var.env]
  kms_key_id     = var.kms_key_ebs[var.env] 
  tags = {
    Name = "ml-efs-z1-01-prod-stage-share"
  }
}

resource "aws_efs_mount_target" "efs-ml-share" {
  file_system_id = aws_efs_file_system.tdm_catalyst_ml_prod_stage_share.id

  subnet_id       = aws_subnet.subnet-z1-app.id
  security_groups = [aws_security_group.efs-sg.id]
}

resource "aws_efs_mount_target" "efs-ml-share-z2" {
  count          =  "${var.env == "dev" ? 0 : 1}"
  file_system_id = aws_efs_file_system.tdm_catalyst_ml_prod_stage_share.id

  subnet_id       = aws_subnet.subnet-z2-app.id
  security_groups = [aws_security_group.efs-sg.id]
}

resource "aws_efs_mount_target" "efs-ml-share-z3" {
  count          =  "${var.env == "dev" ? 0 : 1}"
  file_system_id = aws_efs_file_system.tdm_catalyst_ml_prod_stage_share.id

  subnet_id       = aws_subnet.subnet-z3-app.id
  security_groups = [aws_security_group.efs-sg.id]
}
