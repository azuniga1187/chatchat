# PILOT MGT ENI security group

resource "aws_security_group" "mgt-sg" {
  name        = "${var.env}-mgt-sg"
  description = "ports and protocols for SMTP servers"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-mgt-sg"
  }
}

# PILOT VPN ENI security group

resource "aws_security_group" "vpn-sg" {
  name        = "${var.env}-vpn-sg"
  description = "ports and protocols for VPN servers"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-vpn-sg"
  }
}

# PILOT Private ENI security group

resource "aws_security_group" "private-sg" {
  name        = "${var.env}-private-sg"
  description = "ports and protocols for Private ENIs"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-private-sg"
  }
}

# Marklogic app server security group

resource "aws_security_group" "marklogic-app-sg" {
  name        = "${var.env}-marklogic-app-sg"
  description = "http protocols for use by marklogic app server(s)"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-marklogic-app-sg"
  }
}

# Marklogic ALB  security group

resource "aws_security_group" "marklogic-alb-sg" {
  name        = "${var.env}-marklogic-alb-sg"
  description = "http protocols for use by marklogic application load balancer"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-marklogic-alb-sg"
  }
}

# Marklogic All SG

resource "aws_security_group" "all-sg" {
  name        = "${var.env}-all-sg"
  description = "security group that applies to all instances"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-all-sg"
  }
}

#sg for nifi port 8080 - dev/test only!

resource "aws_security_group" "nifi-8080-sg" {
  name        = "${var.env}-nifi-8080-sg"
  description = "security group that applies to all instances"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-nifi-8080-sg"
  }
}

resource "aws_security_group" "nifi-sg" {
  name        = "${var.env}-nifi-8092-sg"
  description = "security group that applies to all instances"
  vpc_id      = aws_vpc.vpc.id

  tags = {
    Name = "${var.env}-nifi-sg"
  }
}

# Splunk Rules
resource "aws_security_group_rule" "marklogic-ingress-tcp-8089-Splunk" {
  type                     = "ingress"
  from_port                = 8089
  to_port                  = 8089
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.all-sg.id
}

resource "aws_security_group_rule" "marklogic-egress-tcp-8089-Splunk" {
  type                     = "egress"
  from_port                = 8089
  to_port                  = 8089
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.all-sg.id
}

resource "aws_security_group_rule" "marklogic-egress-tcp-9997-Splunk" {
  type                     = "egress"
  from_port                = 9997
  to_port                  = 9997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.all-sg.id
}

# HBSS rules

resource "aws_security_group_rule" "marklogic-ingress-tcp-80-hbss" {
  type                     = "ingress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-8081-hbss" {
  type                     = "ingress"
  from_port                = 8081
  to_port                  = 8081
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-8082-hbss" {
  type                     = "ingress"
  from_port                = 8082
  to_port                  = 8082
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-8083-hbss" {
  type                     = "ingress"
  from_port                = 8083
  to_port                  = 8083
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-591-hbss" {
  type                     = "ingress"
  from_port                = 591
  to_port                  = 591
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-egress-tcp-591-hbss" {
  type                     = "egress"
  from_port                = 591
  to_port                  = 591
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

#Splunk Rules
resource "aws_security_group_rule" "marklogic-ingress-tcp-8089-spdep" {
  type                     = "ingress"
  from_port                = 8089
  to_port                  = 8089
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-9997-spidx" {
  type                     = "ingress"
  from_port                = 9997
  to_port                  = 9997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-egress-tcp-8089-spdep" {
  type                     = "egress"
  from_port                = 8089
  to_port                  = 8089
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-egress-tcp-9997-spidx" {
  type                     = "egress"
  from_port                = 9997
  to_port                  = 9997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}


# NTP rule
resource "aws_security_group_rule" "marklogic-egress-udp-123-NTP" {
  type                     = "egress"
  from_port                = 123
  to_port                  = 123
  protocol                 = "udp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

# ACASS rules

resource "aws_security_group_rule" "marklogic-ingress-tcp-25-ACAS" {
  type                     = "ingress"
  from_port                = 25
  to_port                  = 25
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

# SMTP
resource "aws_security_group_rule" "marklogic-egress-tcp-25-smtp" {
  type                     = "egress"
  from_port                = 25
  to_port                  = 25
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

# MGT ENI security group rules

resource "aws_security_group_rule" "mgt-sg-ingress-ssh-vpn" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "mgt-sg-ingress-https-vpn" {
  type                     = "ingress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "mgt-sg-egress-udp-1812-vpn" {
  type                     = "egress"
  from_port                = 1812
  to_port                  = 1812
  protocol                 = "udp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "mgt-sg-egress-udp-9516-vpn" {
  type                     = "egress"
  from_port                = 9516
  to_port                  = 9516
  protocol                 = "udp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "mgt-sg-egress-udp-123-vpn" {
  type                     = "egress"
  from_port                = 123
  to_port                  = 123
  protocol                 = "udp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "mgt-sg-egress-udp-53-vpn" {
  type                     = "egress"
  from_port                = 53
  to_port                  = 53
  protocol                 = "udp"
  security_group_id        = aws_security_group.mgt-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

# VPN ENI security group rules
resource "aws_security_group_rule" "vpn-sg-egress-udp-500-vdss-z1" {
  count             = "${var.vdss-vpn-z1_ip[var.env] != "" ? 1 : 0}"
  type              = "egress"
  from_port         = 500
  to_port           = 500
  protocol          = "udp"
  security_group_id = aws_security_group.vpn-sg.id
  cidr_blocks       = [var.vdss-vpn-z1_ip[var.env]]
}

resource "aws_security_group_rule" "vpn-sg-egress-udp-500-vdss-z2" {
  count             = "${var.vdss-vpn-z2_ip[var.env] != "" ? 1 : 0}"
  type              = "egress"
  from_port         = 500
  to_port           = 500
  protocol          = "udp"
  security_group_id = aws_security_group.vpn-sg.id
  cidr_blocks       = [var.vdss-vpn-z2_ip[var.env]]
}


resource "aws_security_group_rule" "vpn-sg-egress-esp-vdss-z1" {
  count             = "${var.vdss-vpn-z1_ip[var.env] != "" ? 1 : 0}"
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "50"
  security_group_id = aws_security_group.vpn-sg.id
  cidr_blocks       = [var.vdss-vpn-z1_ip[var.env]]
}

resource "aws_security_group_rule" "vpn-sg-egress-esp-vdss-z2" {
  count             = "${var.vdss-vpn-z2_ip[var.env] != "" ? 1 : 0}"
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "50"
  security_group_id = aws_security_group.vpn-sg.id
  cidr_blocks       = [var.vdss-vpn-z2_ip[var.env]]
}


# Private ENI security group rules
resource "aws_security_group_rule" "private-sg-egress-all" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = aws_security_group.private-sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "private-sg-ingress-all" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  security_group_id = aws_security_group.private-sg.id
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "sg-marklogic-app-sg-tcp-443-s3-endpoint" {
  type              = "egress"
  to_port           = 443
  from_port         = 443
  protocol          = "tcp"
  prefix_list_ids   = [aws_vpc_endpoint.marklogic-vpc-s3-endpoint.prefix_list_id]
  security_group_id = aws_security_group.marklogic-app-sg.id
}

# nosql apparently requires tcp (http) through 7997
resource "aws_security_group_rule" "sg-pilot-marklogic-tcp-7997-egress" {
  type                     = "egress"
  from_port                = 7997
  to_port                  = 7997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "sg-pilot-marklogic-tcp-7997-self-ingress" {
  type                     = "ingress"
  from_port                = 7997
  to_port                  = 7997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

# XDQP protocol security group rules.  Allow ingress, egress for TCP through ports 7998, 7999

resource "aws_security_group_rule" "sg-pilot-xdqp-egress" {
  type                     = "egress"
  from_port                = 7998
  to_port                  = 7999
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "sg-pilot-xdqp-ingress" {
  type                     = "ingress"
  from_port                = 7998
  to_port                  = 7999
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

# HTTP and TCP rules for MarkLogic App servers.  

resource "aws_security_group_rule" "sg-pilot-marklogic-app-8000-8002-ingress" {
  type                     = "ingress"
  from_port                = 8000
  to_port                  = 8002
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "marklogic-app-sg-server-ingress-8001" {
  type                     = "ingress"
  from_port                = 8001
  to_port                  = 8001
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-alb-sg.id
}

resource "aws_security_group_rule" "sg-pilot-marklogic-app-8000-8002-egress" {
  type                     = "egress"
  from_port                = 8000
  to_port                  = 8002
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "sg-pilot-marklogic-app-8010-8011-ingress" {
  type                     = "ingress"
  from_port                = 8010
  to_port                  = 8011
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "sg-pilot-marklogic-app-8010-8011-egress" {
  type                     = "egress"
  from_port                = 8010
  to_port                  = 8011
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

# all-sg security group rules

resource "aws_security_group_rule" "sg-all-private-ingress-22" {
  type                     = "ingress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8092-egress" {
  type                     = "egress"
  from_port                = 8092
  to_port                  = 8092
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8092-ingress" {
  type                     = "ingress"
  from_port                = 8092
  to_port                  = 8092
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-2888-egress" {
  type                     = "egress"
  from_port                = 2888
  to_port                  = 2888
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-2888-ingress" {
  type                     = "ingress"
  from_port                = 2888
  to_port                  = 2888
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-3888-egress" {
  type                     = "egress"
  from_port                = 3888
  to_port                  = 3888
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-3888-ingress" {
  type                     = "ingress"
  from_port                = 3888
  to_port                  = 3888
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8080-egress" {
  type                     = "egress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8080-ingress" {
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8080-ingress-private" {
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "sg-nifi-9443-egress" {
  type                     = "egress"
  from_port                = 9443
  to_port                  = 9443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-9443-ingress-private" {
  type                     = "ingress"
  from_port                = 9443
  to_port                  = 9443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "sg-nifi-9443-ingress-8080" {
  type                     = "ingress"
  from_port                = 9443
  to_port                  = 9443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-8080-sg.id
}

resource "aws_security_group_rule" "sg-nifi-9443-egress-8080" {
  type                     = "egress"
  from_port                = 9443
  to_port                  = 9443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-8080-sg.id
}

resource "aws_security_group_rule" "sg-nifi-9443-ingress" {
  type                     = "ingress"
  from_port                = 9443
  to_port                  = 9443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-2181-egress" {
  type                     = "egress"
  from_port                = 2181
  to_port                  = 2181
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-2181-ingress" {
  type                     = "ingress"
  from_port                = 2181
  to_port                  = 2181
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8181-8182-egress" {
  type                     = "egress"
  from_port                = 8181
  to_port                  = 8182
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8181-8182-ingress" {
  type                     = "ingress"
  from_port                = 8181
  to_port                  = 8182
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8443-private-sg-ingress" {
  type                     = "ingress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8443-private-sg-ingress-nifi" {
  type                     = "ingress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8443-egress-nifi" {
  type                     = "egress"
  from_port                = 8443
  to_port                  = 8443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8092-private-sg-ingress" {
  type                     = "ingress"
  from_port                = 8092
  to_port                  = 8092
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "appian-sg-egress-tcp-puppet" {
  type                     = "egress"
  from_port                = 8140
  to_port                  = 8140
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-sg-egress-tcp-puppet-8142" {
  type                     = "egress"
  from_port                = 8142
  to_port                  = 8142
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-sg-ad-server-egress-dns" {
  type                     = "egress"
  from_port                = 53
  to_port                  = 53
  protocol                 = "udp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-sg-ad-server-egress-tcp-589-AD" {
  type                     = "egress"
  from_port                = 589
  to_port                  = 589
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.all-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-80" {
  type                     = "egress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-443" {
  type                     = "egress"
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-alb-sg-server-egress-8001" {
  type                     = "egress"
  from_port                = 8001
  to_port                  = 8001
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "marklogic-alb-sg-server-ingress-8001-private-sg" {
  type                     = "ingress"
  from_port                = 8001
  to_port                  = 8001
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-alb-sg-server-ingress-8001" {
  type                     = "ingress"
  from_port                = 8001
  to_port                  = 8001
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.marklogic-alb-sg.id
}

# rule added to support the load balancer usmc catalyst app connection from Appian
resource "aws_security_group_rule" "marklogic-alb-sg-server-ingress-8010-private-sg" {
  type                     = "ingress"
  from_port                = 8010
  to_port                  = 8010
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-alb-sg-server-egress-8010" {
  type                     = "egress"
  from_port                = 8010
  to_port                  = 8010
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "marklogic-app-sg-server-ingress-8010-alb-sg" {
  type                     = "ingress"
  from_port                = 8010
  to_port                  = 8010
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-alb-sg.id
}
#rules added to support the load balancer health check via tcp port 7997.

resource "aws_security_group_rule" "marklogic-alb-sg-server-egress-7997" {
  type                     = "egress"
  from_port                = 7997
  to_port                  = 7997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.marklogic-app-sg.id
}

resource "aws_security_group_rule" "marklogic-app-sg-server-ingress-7997" {
  type                     = "ingress"
  from_port                = 7997
  to_port                  = 7997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.marklogic-alb-sg.id
}

# Ports supporting communication from F5 to Mark Logic 

resource "aws_security_group_rule" "marklogic-app-sg-server-private-sg-ingress-7997" {
  type                     = "ingress"
  from_port                = 7997
  to_port                  = 7997
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-ingress-tcp-8010-private" {
  type                     = "ingress"
  from_port                = 8000
  to_port                  = 8020
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-app-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

#late edition - ldap rule from all-sg to private

resource "aws_security_group_rule" "marklogic-all-sg-egress-389-private" {
  type                     = "egress"
  from_port                = 389
  to_port                  = 389
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "marklogic-nifi-sg-egress-389-private" {
  type                     = "egress"
  from_port                = 389
  to_port                  = 389
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

# all-sg rules added for AD/SSSD 
# Port 389 TCP was already present
# resource "aws_security_group_rule" "all-sg-server-egress-ldap-tcp-389" {
#   type                     = "egress"
#   from_port                = 389
#   to_port                  = 389
#   protocol                 = "tcp"
#   security_group_id        = aws_security_group.all-sg.id
#   source_security_group_id = aws_security_group.private-sg.id
# }

resource "aws_security_group_rule" "all-sg-server-egress-ldap-udp-389" {
  type                     = "egress"
  from_port                = 389
  to_port                  = 389
  protocol                 = "udp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-ldap-tcp-636" {
  type                     = "egress"
  from_port                = 636
  to_port                  = 636
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-krb-tcp-88" {
  type                     = "egress"
  from_port                = 88
  to_port                  = 88
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-krb-udp-88" {
  type                     = "egress"
  from_port                = 88
  to_port                  = 88
  protocol                 = "udp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-krb-tcp-464" {
  type                     = "egress"
  from_port                = 464
  to_port                  = 464
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-krb-udp-464" {
  type                     = "egress"
  from_port                = 464
  to_port                  = 464
  protocol                 = "udp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-ldapgc-tcp-3268" {
  type                     = "egress"
  from_port                = 3268
  to_port                  = 3268
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-ldapgc-tcp-3269" {
  type                     = "egress"
  from_port                = 3269
  to_port                  = 3269
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "all-sg-server-egress-ephem-tcp-49152-65535" {
  type                     = "egress"
  from_port                = 49152
  to_port                  = 65535
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}

resource "aws_security_group_rule" "sg-nifi-22-SFG-egress" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  cidr_blocks              = [var.disa_sfg_ip[var.env]]
  security_group_id        = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-22-GCSS-egress" {
  type                     = "egress"
  from_port                = 22
  to_port                  = 22
  protocol                 = "tcp"
  cidr_blocks              = [var.gcss_ip[var.env]]
  security_group_id        = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8080-private-sg-ingress" {
  type                     = "ingress"
  from_port                = 8080
  to_port                  = 8080
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-389-private-sg-egress" {
  type                     = "egress"
  from_port                = 389
  to_port                  = 389
  protocol                 = "tcp"
  security_group_id        = aws_security_group.private-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8010-8011-private-sg-egress" {
  type                     = "egress"
  from_port                = 8010
  to_port                  = 8011
  protocol                 = "tcp"
  security_group_id        = aws_security_group.marklogic-alb-sg.id
  source_security_group_id = aws_security_group.nifi-sg.id
}

resource "aws_security_group_rule" "sg-nifi-8010-8011-marklogic-alb-sg-egress" {
  type                     = "egress"
  from_port                = 8010
  to_port                  = 8011
  protocol                 = "tcp"
  security_group_id        = aws_security_group.nifi-sg.id
  source_security_group_id = aws_security_group.marklogic-alb-sg.id
}

resource "aws_security_group_rule" "all-sg-server-igress-monitoring-tcp-9998-9999" {
  type                     = "ingress"
  from_port                = 9998
  to_port                  = 9999
  protocol                 = "tcp"
  security_group_id        = aws_security_group.all-sg.id
  source_security_group_id = aws_security_group.private-sg.id
}