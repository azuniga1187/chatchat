#S3 Buckets for Marklogic Backup
resource "aws_s3_bucket" "mldb-backup-env" {
  bucket = "mldb-backup-${var.env}"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "mldb-backup-env_public_access_block" {
  bucket = "${aws_s3_bucket.mldb-backup-env.id}"

  block_public_acls   = true
  block_public_policy = true
}

resource "aws_s3_bucket" "nifi-generatedFiles-env" {
  bucket = "nifi-generated-files-${var.env}"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "nifi-generatedFiles-env_public_access_block" {
  bucket = "${aws_s3_bucket.nifi-generatedFiles-env.id}"

  block_public_acls   = true
  block_public_policy = true
}