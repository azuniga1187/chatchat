locals {
    eid                   = substr(var.env, 0, 1)

  groupsall    = [
    "apachelog",
    "awslog",
    "auditlog",
    "cloudwatchlog",
    "applog",
    "syslog"
  ]

  groupsawslog    = [
    "awslog"
  ]

  groupsauditlog    = [
    "auditlog"
  ]

  groupscwlog    = [
    "cloudwatchlog"
  ]
  groupssyslog  = [
    "syslog"
  ]

  groupsapachelog = [
    "apachelog"
  ]

  groupsapplog = [
    "applog"
  ]

  streamsall = [""]

  streamsauditlog = [
    "s-ml-nifi-101.core.busops.usmc.mil",
    "s-ml-nifi-201.core.busops.usmc.mil",
    "s-ml-nifi-301.core.busops.usmc.mil",
    "s-ml-db-101.core.busops.usmc.mil",
    "s-ml-db-201.core.busops.usmc.mil",
    "s-ml-db-301.core.busops.usmc.mil"
  ]
    
  streamscwlog = [
    "s-ml-nifi-101.core.busops.usmc.mil",
    "s-ml-nifi-201.core.busops.usmc.mil",
    "s-ml-nifi-301.core.busops.usmc.mil",
    "s-ml-db-101.core.busops.usmc.mil",
    "s-ml-db-201.core.busops.usmc.mil",
    "s-ml-db-301.core.busops.usmc.mil"
  ]

  streamssyslog = [
    "s-ml-nifi-101.core.busops.usmc.mil",
    "s-ml-nifi-201.core.busops.usmc.mil",
    "s-ml-nifi-301.core.busops.usmc.mil",
    "s-ml-db-101.core.busops.usmc.mil",
    "s-ml-db-201.core.busops.usmc.mil",
    "s-ml-db-301.core.busops.usmc.mil"
  ]

  streamsapachelog = [
    "s-ml-db-101.core.busops.usmc.mil",
    "s-ml-db-201.core.busops.usmc.mil",
    "s-ml-db-301.core.busops.usmc.mil"
  ]

  streamsapplog = [
    "s-ml-db-101.core.busops.usmc.mil",
    "s-ml-db-201.core.busops.usmc.mil",
    "s-ml-db-301.core.busops.usmc.mil",
    "s-ml-nifi-101.core.busops.usmc.mil",
    "s-ml-nifi-201.core.busops.usmc.mil",
    "s-ml-nifi-301.core.busops.usmc.mil"
  ]

  ##SubsciptionFilters
  subscriptionsall = [
    "${local.eid}-${var.customer-name}-awslog-filter",
    "${local.eid}-${var.customer-name}-apachelog-filter",
  ]

  subscriptionsawslog = [
    "${local.eid}-${var.customer-name}-awslog-filter"
  ]
  subscriptionsapachelog = [
    "${local.eid}-${var.customer-name}-apachelog-filter"
  ]

 ##All
  groups_streamsall = distinct(flatten([
    for groupsall in local.groupsall : [
      for streamsall in local.streamsall : {
        streamsall = streamsall
        groupsall  = groupsall
      }
    ]
  ]))

  ##Auditlog
   groups_streamsauditlog = distinct(flatten([
    for groupsauditlog in local.groupsauditlog : [
      for streamsauditlog in local.streamsauditlog : {
        streamsauditlog = streamsauditlog
        groupsauditlog  = groupsauditlog
      }
    ]
  ]))

  #CWlog
  groups_streamscwlog = distinct(flatten([
    for groupscwlog in local.groupscwlog : [
      for streamscwlog in local.streamscwlog : {
        streamscwlog = streamscwlog
        groupscwlog  = groupscwlog
      }
    ]
  ]))

  ##Syslog
  groups_streamssyslog = distinct(flatten([
    for groupssyslog in local.groupssyslog : [
      for streamssyslog in local.streamssyslog : {
        streamssyslog = streamssyslog
        groupssyslog  = groupssyslog
      }
    ]
  ]))

  ##Apachelog
  groups_streamsapachelog = distinct(flatten([
    for groupsapachelog in local.groupsapachelog : [
      for streamsapachelog in local.streamsapachelog : {
        streamsapachelog = streamsapachelog
        groupsapachelog  = groupsapachelog
      }
    ]
  ]))

  ##Applog
  groups_streamsapplog = distinct(flatten([
    for groupsapplog in local.groupsapplog : [
      for streamsapplog in local.streamsapplog : {
        streamsapplog = streamsapplog
        groupsapplog  = groupsapplog
      }
    ]
  ]))

  #ApachelogSubFilter
  groups_subscriptionsapachelog = distinct(flatten([
    for groupsapachelog in local.groupsapachelog : [
      for subscriptionsapachelog in local.subscriptionsapachelog : {
        subscriptionsapachelog = subscriptionsapachelog
        groupsapachelog        = groupsapachelog
      }
    ]
  ]))
}
