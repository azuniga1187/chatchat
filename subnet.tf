#  Subnet subnet-z1-perimeter

resource "aws_subnet" "subnet-z1-perimeter" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z1-perimeter-cidr[var.env]

  availability_zone = var.availability_zone_1

  tags = {
    Name = "${var.env}-subnet-z1-perimeter"
  }
}

#  Subnet subnet-z1-private

resource "aws_subnet" "subnet-z1-private" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z1-private-cidr[var.env]

  availability_zone = var.availability_zone_1

  tags = {
    Name = "${var.env}-subnet-z1-private"
  }
}

#  Subnet subnet-z2-mgt

resource "aws_subnet" "subnet-z1-mgt" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z1-mgt-cidr[var.env]

  availability_zone = var.availability_zone_1

  tags = {
    Name = "${var.env}-subnet-z1-mgt"
  }
}

#  Subnet subnet-z2-perimeter

resource "aws_subnet" "subnet-z2-perimeter" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z2-perimeter-cidr[var.env]

  availability_zone = var.availability_zone_2

  tags = {
    Name = "${var.env}-subnet-z2-perimeter"
  }
}

#  Subnet subnet-z2-private

resource "aws_subnet" "subnet-z2-private" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z2-private-cidr[var.env]

  availability_zone = var.availability_zone_2

  tags = {
    Name = "${var.env}-subnet-z2-private"
  }
}

#  Subnet subnet-z2-mgt

resource "aws_subnet" "subnet-z2-mgt" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z2-mgt-cidr[var.env]

  availability_zone = var.availability_zone_2

  tags = {
    Name = "${var.env}-subnet-z2-mgt"
  }
}

#  Subnet subnet-z3-perimeter

resource "aws_subnet" "subnet-z3-perimeter" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z3-perimeter-cidr[var.env]

  availability_zone = var.availability_zone_3

  tags = {
    Name = "${var.env}-subnet-z3-perimeter"
  }
}

#  Subnet subnet-z3-private

resource "aws_subnet" "subnet-z3-private" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z3-private-cidr[var.env]

  availability_zone = var.availability_zone_3

  tags = {
    Name = "${var.env}-subnet-z3-private"
  }
}

#  Subnet subnet-z3-mgt

resource "aws_subnet" "subnet-z3-mgt" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z3-mgt-cidr[var.env]

  availability_zone = var.availability_zone_3

  tags = {
    Name = "${var.env}-subnet-z3-mgt"
  }
}

# Subnet subnet-z1-app

resource "aws_subnet" "subnet-z1-app" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z1-app-cidr[var.env]

  availability_zone = var.availability_zone_1

  tags = {
    Name = "${var.env}-subnet-z1-app"
  }
}

# Subnet subnet-z2-app

resource "aws_subnet" "subnet-z2-app" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z2-app-cidr[var.env]

  availability_zone = var.availability_zone_2

  tags = {
    Name = "${var.env}-subnet-z2-app"
  }
}

# Subnet subnet-z3-app

resource "aws_subnet" "subnet-z3-app" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z3-app-cidr[var.env]

  availability_zone = var.availability_zone_3

  tags = {
    Name = "${var.env}-subnet-z3-app"
  }
}

# Subnet subnet-z1-db

resource "aws_subnet" "subnet-z1-db" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z1-db-cidr[var.env]

  availability_zone = var.availability_zone_1

  tags = {
    Name = "${var.env}-subnet-z1-db"
  }
}

# Subnet subnet-z2-db

resource "aws_subnet" "subnet-z2-db" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z2-db-cidr[var.env]

  availability_zone = var.availability_zone_2

  tags = {
    Name = "${var.env}-subnet-z2-db"
  }
}

# Subnet subnet-z3-db

resource "aws_subnet" "subnet-z3-db" {
  vpc_id = aws_vpc.vpc.id

  cidr_block = var.z3-db-cidr[var.env]

  availability_zone = var.availability_zone_3

  tags = {
    Name = "${var.env}-subnet-z3-db"
  }
}

