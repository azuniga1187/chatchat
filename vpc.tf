resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr[var.env]
  enable_dns_hostnames = true

  tags = {
    Name = var.vpc_name[var.env]
  }
}

resource "aws_vpc_dhcp_options" "marklogic_dev_dopt" {
  domain_name_servers = var.ad-z1-01_privateip[var.env]
  ntp_servers         = var.ad-z1-01_privateip[var.env]
  domain_name         = var.domain_name[var.env]
}

resource "aws_vpc_dhcp_options_association" "marklogic_dev_dopt" {
  vpc_id          = aws_vpc.vpc.id
  dhcp_options_id = aws_vpc_dhcp_options.marklogic_dev_dopt.id
}

resource "aws_vpc_peering_connection" "pcx" {
  vpc_id        = aws_vpc.vpc.id
  peer_owner_id = var.vdss_account_id      #Provided by MCBOSS
  peer_vpc_id   = var.vdss_vpc_id[var.env] #Provided by MCBOSS
}

#S3 Endpoint
resource "aws_vpc_endpoint" "marklogic-vpc-s3-endpoint" {
    auto_accept = true
    vpc_id = aws_vpc.vpc.id
    service_name = "com.amazonaws.us-gov-west-1.s3"
    route_table_ids = ["${aws_route_table.rt-z1.id}", "${aws_route_table.rt-z2.id}", "${aws_route_table.rt-z3.id}"]
    policy = <<POLICY
{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Action": "*",
            "Effect": "Allow",
            "Resource": "*",
            "Principal": "*"
        }
    ]
}
POLICY
}
