# VPC region

variable "region" {
  default = "us-gov-west-1"
}

# VPC name

variable "vpc_name" {
  type        = map(string)
  default = {
    "prod"    = "prod-marklogic"
    "dev"     = "marklogic"
    "staging" = "staging-marklogic"
  }
}

#Customer 

variable "customer-name" {
  type    = string
  default = "marklogic"
}


# Key pairs

variable "aws_key_name" {
  description = "Name of the key pair for host access"
  default     = "marklogic"
}

variable "snapshot-retention-parameter" {
  default = "7,4,12,1"  
}

# VDSS VPC peering ID

# VDSS Account ID
variable "vdss_account_id" {
  description = "Account where VDSS VPC resides"
  default     = "781228292447"
}
# VDSS VPC ID
variable "vdss_vpc_id" {
  description = "VDSS VPC ID"
  type        = map(string)
  default = {
    "prod"    = "vpc-43a08a26"
    "dev"     = "vpc-7ef0a71b"
    "staging" = "vpc-43a08a26"
  }
}

variable "vdss_pcx" {
  description = "Name of the VDSS peering connection"
  default     = {
    "dev" = "pcx-1725017e"
    "staging" = "10.0.108.0/22"
    "prod"    = "10.0.112.0/22"
  }
}

# Availability zone variables

variable "availability_zone_1" {
  description = "Name of zone 1"
  default     = "us-gov-west-1c"
}

variable "availability_zone_2" {
  description = "Name of zone 2"
  default     = "us-gov-west-1b"
}

variable "availability_zone_3" {
  description = "Name of zone 3"
  default     = "us-gov-west-1a"
}

# Default Juniper vSRX 18.4R1 AMI for us-gov-west-1

variable "vsrx_default_ami" {
  default = "ami-a26e01c3"
}

# Default Juniper vSRX 19.3r2 BYOL AMI for us-gov-west-1

variable "vsrx_default_ami_v193r2_byol" {
  default = "ami-f7466496"
}

# Default RHEL 7 base AMI for us-gov-west-1

variable "linux_default_ami" {
  default = "ami-0466e865"
}

# Default Server 2016 base AMI for us-gov-west-1

variable "windows_default_ami" {
  default = "ami-d761f6b6"
}

# Default Server 2012R2 base AMI for us-gov-west-1

variable "windows2012r2_default_ami" {
  default = "ami-1329ba72"
}

# VPC and Subnet variables

variable "vpc_cidr" {
  description = "IP CIDR block for the VPC"
  default     = {
      "dev" = "10.0.56.0/22"
      "staging" = "10.0.108.0/22"
      "prod"  = "10.0.112.0/22"
  }
}

variable "z1-perimeter-cidr" {
  description = "IP CIDR block for z1-perimeter"
  default     = {
    "dev" = "10.0.56.0/28"
    "staging" = "10.0.108.0/28"
    "prod"    = "10.0.112.0/28"
  }
}

variable "z1-mgt-cidr" {
  description = "IP CIDR block for z1-mgt"
  default     = {
   "dev" = "10.0.56.16/28"
   "staging" = "10.0.108.16/28"
   "prod"    = "10.0.112.16/28"
  }
}

variable "z1-private-cidr" {
  description = "IP CIDR block for z1-private"
  default     = {
   "dev" = "10.0.56.32/28"
   "staging" = "10.0.108.32/28"
   "prod"    = "10.0.112.32/28"
  }
}

variable "z1-db-cidr" {
  description = "IP CIDR block for z1-db"
  default     = {
     "dev" = "10.0.56.64/27"
     "staging" = "10.0.108.64/27"
     "prod"   =  "10.0.112.64/27"
  }
}

variable "z1-app-cidr" {
  description = "IP CIDR block for z1-app"
  default     = {
    "dev" =  "10.0.56.96/27"
    "staging" = "10.0.108.96/27"
    "prod"    = "10.0.112.96/27"
  }
}

variable "z2-perimeter-cidr" {
  description = "IP CIDR block for z2-perimeter"
  default     = {
    "dev" = "10.0.57.0/28"
    "staging" = "10.0.109.0/28"
    "prod"    = "10.0.113.0/28"
  }
}

variable "z2-mgt-cidr" {
  description = "IP CIDR block for z2-mgt"
  default     = {
   "dev" = "10.0.57.16/28"
   "staging" = "10.0.109.16/28"
   "prod"    = "10.0.113.16/28"
  }
}

variable "z2-private-cidr" {
  description = "IP CIDR block for z2-private"
  default     = {
   "dev" = "10.0.57.32/28"
   "staging" = "10.0.109.32/28"
   "prod"    = "10.0.113.32/28"
  }
}

variable "z2-db-cidr" {
  description = "IP CIDR block for z2-db"
  default     = {
     "dev" = "10.0.57.64/27"
     "staging" = "10.0.109.64/27"
     "prod"   =  "10.0.113.64/27"
  }
}

variable "z2-app-cidr" {
  description = "IP CIDR block for z2-app"
  default     = {
    "dev" =  "10.0.57.96/27"
    "staging" = "10.0.109.96/27"
    "prod"    = "10.0.113.96/27"
  }
}

variable "z3-perimeter-cidr" {
  description = "IP CIDR block for z3-perimeter"
  default     = {
    "dev" = "10.0.58.0/28"
    "staging" = "10.0.110.0/28"
    "prod"    = "10.0.114.0/28"
  }
}

variable "z3-mgt-cidr" {
  description = "IP CIDR block for z3-mgt"
  default     = {
   "dev" = "10.0.58.16/28"
   "staging" = "10.0.110.16/28"
   "prod"    = "10.0.114.16/28"
  }
}

variable "z3-private-cidr" {
  description = "IP CIDR block for z3-private"
  default     = {
   "dev" = "10.0.58.32/28"
   "staging" = "10.0.110.32/28"
   "prod"    = "10.0.114.32/28"
  }
}

variable "z3-db-cidr" {
  description = "IP CIDR block for z3-db"
  default     = {
     "dev" = "10.0.58.64/27"
     "staging" = "10.0.110.64/27"
     "prod"   =  "10.0.114.64/27"
  }
}

variable "z3-app-cidr" {
  description = "IP CIDR block for z3-app"
  default     = {
    "dev" =  "10.0.58.96/27"
    "staging" = "10.0.110.96/27"
    "prod"    = "10.0.114.96/27"
  }
}

# Instance type variables

variable "ml_instance_type" {
  type    = map(string)
  default = {
    "dev"     = "r5.4xlarge"
    "staging" = "r5.4xlarge"
    "prod"    = "r5.8xlarge"
  }  
}

# Instance name variables

variable "vpn-z1-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-vpn-101"
    "staging" = "s-ml-vpn-101"
    "prod"    = "p-ml-vpn-101"
  }
}

variable "vpn-z2-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-vpn-201"
    "staging" = "s-ml-vpn-201"
    "prod"    = "p-ml-vpn-201"
  }
}

variable "vpn-z3-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-vpn-301"
    "staging" = "s-ml-vpn-301"
    "prod"    = "p-ml-vpn-301"
  }
}

variable "marklogic-z1-01_name" {
  type    = string
  default = "marklogic-z1-01"
}

# marklogic nosql z1 01 name
variable "marklogicnosql-z1-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-101"
    "staging" = "s-ml-db-101"
    "prod"    = "p-ml-db-101"
  }
}

# marklogic nosql z2 01 name
variable "marklogicnosql-z2-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-201"
    "staging" = "s-ml-db-201"
    "prod"    = "p-ml-db-201"
  }
}

# marklogic nosql z3 01 name
variable "marklogicnosql-z3-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-301"
    "staging" = "s-ml-db-301"
    "prod"    = "p-ml-db-301"
  }
}

# marklogic nosql z1 02 name
variable "marklogicnosql-z1-02_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-102"
    "staging" = "s-ml-db-102"
    "prod"    = "p-ml-db-102"
  }
}

# marklogic nosql z2 02 name
variable "marklogicnosql-z2-02_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-202"
    "staging" = "s-ml-db-202"
    "prod"    = "p-ml-db-202"
  }
}


# marklogic nosql z3 02 name
variable "marklogicnosql-z3-02_name" {
 type    = map(string)
  default     = {
    "dev"     = "d-ml-db-302"
    "staging" = "s-ml-db-302"
    "prod"    = "p-ml-db-302"
  }
}

# marklogic nosql z1 03 name
variable "marklogicnosql-z1-03_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-103"
    "staging" = "s-ml-db-103"
    "prod"    = "p-ml-db-103"
  }
}

# marklogic nosql z2 03 name
variable "marklogicnosql-z2-03_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-203"
    "staging" = "s-ml-db-203"
    "prod"    = "p-ml-db-203"
  }
}

# marklogic nosql z3 03 name
variable "marklogicnosql-z3-03_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-db-303"
    "staging" = "s-ml-db-303"
    "prod"    = "p-ml-db-303"
  }
}

# marklogic apache nifi z1 01 name
variable "marklogicnifi-z1-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-nifi-101"
    "staging" = "s-ml-nifi-101"
    "prod"    = "p-ml-nifi-101"
  }
}

# marklogic apache nifi z2 01 name
variable "marklogicnifi-z2-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-nifi-201"
    "staging" = "s-ml-nifi-201"
    "prod"    = "p-ml-nifi-201"
  }
}

# marklogic apache nifi z3 01 name
variable "marklogicnifi-z3-01_name" {
  type    = map(string)
  default     = {
    "dev"     = "d-ml-nifi-301"
    "staging" = "s-ml-nifi-301"
    "prod"    = "p-ml-nifi-301"
  }
}

# Instance IP address variables

variable "eni-vpn-z1-01-perimeter-ip" {
  description = "IP for the vpn-z1-01 Perimeter ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.4"
    "staging" = "10.0.108.4"
    "prod"    = "10.0.112.4"
  }
}

variable "eni-vpn-z1-01-mgt-ip" {
  description = "IP for the vpn-z1-01 MGT ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.20"
    "staging" = "10.0.108.20"
    "prod"    = "10.0.112.20"
  }
}

variable "eni-vpn-z1-01-private-ip" {
  description = "IP for the vpn-z1-01 Private ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.36"
    "staging" = "10.0.108.36"
    "prod"    = "10.0.112.36"
  }
}

# VPN Z2 Ip address variables

variable "eni-vpn-z2-01-perimeter-ip" {
  description = "IP for the vpn-z2-01 Perimeter ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.57.4"
    "staging" = "10.0.109.4"
    "prod"    = "10.0.113.4"
  }
}

variable "eni-vpn-z2-01-mgt-ip" {
  description = "IP for the vpn-z2-01 MGT ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.57.20"
    "staging" = "10.0.109.20"
    "prod"    = "10.0.113.20"
  }
}

variable "eni-vpn-z2-01-private-ip" {
  description = "IP for the vpn-z2-01 Private ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.57.36"
    "staging" = "10.0.109.36"
    "prod"    = "10.0.113.36"
  }
}

# VPN z3 Ip address variables

variable "eni-vpn-z3-01-perimeter-ip" {
  description = "IP for the vpn-z3-01 Perimeter ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.4"
    "staging" = "10.0.110.4"
    "prod"    = "10.0.114.4"
  }
}

variable "eni-vpn-z3-01-mgt-ip" {
  description = "IP for the vpn-z3-01 MGT ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.20"
    "staging" = "10.0.110.20"
    "prod"    = "10.0.114.20"
  }
}

variable "eni-vpn-z3-01-private-ip" {
  description = "IP for the vpn-z3-01 Private ENI"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.36"
    "staging" = "10.0.110.36"
    "prod"    = "10.0.114.36"
  }
}

# MGT instance IP variable

variable "vdss-mgt-ip" {
  description = "IP for the VDSS MGT ENI"
  type        = string
  default     = "10.0.8.121/32"
}

# VDSS VPN instance MGT IP variable

variable "vdss-vpn-z1_ip" {
  description = "IP for the VDSS VPN Z1 ENI"
  type        = map(string)
  default = {
    "prod"    = "10.0.4.128/27"
    "dev"     = "10.0.8.68/32"
    "staging" = "10.0.4.128/27"
  }
}

variable "vdss-vpn-z2_ip" {
  description = "IP for the VDSS VPN Z2 ENI"
  type        = map(string)
  default = {
    "prod"    = "10.0.5.128/27"
    "dev"     = ""
    "staging" = "10.0.5.128/27"
  }
}


#marklogic app server IP

variable "marklogic-z1-01-private-ip" {
  description = "internal IP for the marklogic app server"
  type        = string
  default     = "10.0.56.196" #<= just an initial arbitrary swag  tbd:  derive actual ip
}

#marklogic nosql server z1 01 IP

variable "marklogicnosql-z1-01-private-ip" {
  description = "internal IP for marklogic nosql z1 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.70"
    "staging" = "10.0.108.70"
    "prod"    = "10.0.112.70"
  }
}



#marklogic nosql server z2 01 IP

variable "marklogicnosql-z2-01-private-ip" {
  description = "internal IP for marklogic nosql z2 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.57.70"
    "staging" = "10.0.109.70"
    "prod"    = "10.0.113.70"
  }
}

#marklogic nosql server z3 01 IP

variable "marklogicnosql-z3-01-private-ip" {
  description = "internal IP for marklogic nosql z3 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.70"
    "staging" = "10.0.110.70"
    "prod"    = "10.0.114.70"
  }
}

#marklogic nosql server z1 02 IP

variable "marklogicnosql-z1-02-private-ip" {
  description = "internal IP for marklogic nosql z1 02 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.71"
    "staging" = "10.0.108.71"
    "prod"    = "10.0.112.71"
  }
}

#marklogic nosql server z2 02 IP

variable "marklogicnosql-z2-02-private-ip" {
  description = "internal IP for marklogic nosql z2 02 server"
  type        = map(string)
 default     = {
    "dev"     = "10.0.57.71"
    "staging" = "10.0.109.71"
    "prod"    = "10.0.113.71"
  }
}

#marklogic nosql server z3 02 IP

variable "marklogicnosql-z3-02-private-ip" {
  description = "internal IP for marklogic nosql z3 02 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.71"
    "staging" = "10.0.110.71"
    "prod"    = "10.0.114.71"
  }
}

# #marklogic nosql server z1 03 IP

# variable "marklogicnosql-z1-03-private-ip" {
#   description = "internal IP for marklogic nosql z1 03 server"
#   type        = string
#   default     = "10.0.56.230" #<= just an initial arbitrary sqag tbd: derive actual ip
# }

# #marklogic nosql server z2 03 IP

# variable "marklogicnosql-z2-03-private-ip" {
#   description = "internal IP for marklogic nosql z1 03 server"
#   type        = string
#   default     = "10.0.108.230" 
# }

# #marklogic nosql server z3 03 IP

# variable "marklogicnosql-z3-03-private-ip" {
#   description = "internal IP for marklogic nosql z3 03 server"
#   type        = string
#   default     = "10.0.112.230"
#}

variable "marklogicnifi-z1-01-private-ip" {
  description = "internal IP for marklogic apache nifi z1 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.56.101"
    "staging" = "10.0.108.101"
    "prod"    = "10.0.112.101"
  }
}

#marklogic apache nifi server z2 01 IP

variable "marklogicnifi-z2-01-private-ip" {
  description = "internal IP for marklogic apache nifi z1 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.57.101"
    "staging" = "10.0.109.101"
    "prod"    = "10.0.113.101"
  }
}

#marklogic apache nifi server z3 01 IP

variable "marklogicnifi-z3-01-private-ip" {
  description = "internal IP for marklogic apache nifi z3 01 server"
  type        = map(string)
  default     = {
    "dev"     = "10.0.58.101"
    "staging" = "10.0.110.101"
    "prod"    = "10.0.114.101"
  }
}

variable "ad-z1-01_privateip" {
  description = "IP for AD-Z1-01"
  type        = map(list(string))
  default     = {
    "dev"     = ["10.0.12.41"]
    "staging" = ["10.0.0.36", "10.0.1.36"]
    "prod"    = ["10.0.0.36", "10.0.1.36"]
  }
}

variable "domain_name" {
  description = "Domain name for DHCP options"
  type        = map(string)
  default     = {
    "dev"     = "mcboss.net"
    "staging" = "core.busops.usmc.mil"
    "prod"    = "core.busops.usmc.mil"
  }
}

variable "env" {
  description = "marklogic environment we are building.  Values should be one of 'dev', 'staging', or 'prod'"
  type	      = string
  default     = "dev"
}

variable "nifi_instance_type" {
  description = "Domain name for DHCP options"
  type        = map(string)
  default     = {
    "dev"     = "t2.2xlarge"
    "staging" = "m5.2xlarge"
    "prod"    = "m5.2xlarge"
  }
}

variable "ebs_encryption" {
  description = "EBS Encryption"
  type        = map(string)
  default     = {
    "dev"     = "false"
    "staging" = "true"
    "prod"    = "true"
  }
}

variable "kms_key_ebs" {
  description = "KMS EBS Key"
  type        = map(string)
  default     = {
    "dev"     = "arn:aws-us-gov:kms:us-gov-west-1:709005597329:key/1250bd88-ff84-4b77-bc64-63f4bdd9daf6"
    "staging" = "arn:aws-us-gov:kms:us-gov-west-1:709005597329:key/1250bd88-ff84-4b77-bc64-63f4bdd9daf6"
    "prod"    = "arn:aws-us-gov:kms:us-gov-west-1:709005597329:key/49cf112e-7740-4478-8385-2fc7aa6dc15f"
  }
}

### External Data Connection IP addresses
variable "gcss_ip" {
  description = "IP for the GCSS-MC Connection"
  type        = map(string)
  default = {
    "prod"    = "214.43.160.9/32"
    "dev"     = ""
    "staging" = "214.43.162.10/32"
  }
}

variable "disa_sfg_ip" {
  description = "IP for the DISA SFG Connection"
  type        = map(string)
  default = {
    "prod"    = "156.112.100.250/32"
    "dev"     = ""
    "staging" = "156.112.100.250/32"
  }
}

# Juniper Bootstrap Variables
### These Variables were part of a test, not actively used ###

variable "juniper_radius_secret" {
  type = map(string)
  default = {
    "dev"     = ""
    "staging" = "3onUqmTsTH5X5o!7J8ItBG@iSpV"
    "prod"    = ""
  }
}

variable "juniper_ipsec_psk_z1" {
  type = map(string)
  default = {
    "dev"     = "Az2nXzrKxwvEUdpW6TKM"
    "staging" = "9k1xTFjEvqpYiZR3vu1h"
    "prod"    = "Eni1os2VsyU0nZf450fy"
  }
}

variable "juniper_ipsec_psk_z2" {
  type = map(string)
  default = {
    "dev"     = "HhRWzxPs0uXp9u9akgnZ"
    "staging" = "mIIpft5IkU8vPNAJg5zN"
    "prod"    = "gJUhqFZeLfWHKcA5OirB"
  }
}

variable "juniper_ipsec_psk_z3" {
  type = map(string)
  default = {
    "dev"     = "xKIltDAiIz6WWVxjLPaj"
    "staging" = "Jaa4PHBs6pA5Uz66AN9s"
    "prod"    = "wKYrUYNWVEwsvYBC0q4K"
  }
}

variable "juniper_license_key_z1" {
  type = map(string)
  default = {
    "staging" = "JUNOS668981049 aeaqib qbo4fr emjvgy 3tmnzs haytam bqfvdd aqrxgu gawr2f j5bukt suebge yqyhfd rlhpve vi6baf 3qoa3v jchvcj xm2abj 7sk2zg i3y47a wjop3p n6jn5f taj6j5 cvqgdj 4"
    "prod"    = ""
  }
}

variable "juniper_license_key_z2" {
  type = map(string)
  default = {
    "staging" = "JUNOS434215425 aeaqib qbo4fr emjvgy 3tmnzs haytam bqfuyx mqstga gawr2f j5bukt suebge yqyhfb ocatlo esx5dv aboora 4zfpaw w6hfwk pjosrk vc2uhk m2cnvj iydxut qmoh7v e7aupz g"
    "prod"    = ""
  }
}

variable "juniper_license_key_z3" {
  type = map(string)
  default = {
    "dev"     = ""
    "staging" = "JUNOS530288017 aeaqib qbo4fr emjvgy 3tmnzs haytam bqfvbw susbky gawr2f j5bukt suebge yqyhfb xheyf5 x45lft 332iip etvq3b 6hsbn5 bw5nel 76odda atksd2 eqn5ut u545dk 4qeyho m"
    "prod"    = ""
  }
}

variable "usmc_recipient_arn" {
  description = "Opensearch log destination"
  type        = string
  default     = "arn:aws-us-gov:logs:us-gov-west-1:161871440902:destination:usmc"
  }

variable "flowlog_destination_arn" {
  description = "Flowlog CW log destination"
  type        = string
  default     = "arn:aws-us-gov:logs:us-gov-west-1:709005597329:log-group:awslog:*"
  }

variable "flowlog_iam_arn" {
  description = "Flowlog CW log destination"
  type        = string
  default     = "arn:aws-us-gov:iam::709005597329:role/IL4_flow_logs_role"
  }