#marklogic 
resource "aws_iam_user" "mldb-backup-env-user" {
  name = "mldb-backup-${var.env}-user"
}

resource "aws_iam_group" "mldb-backup-env-group" {
  name = "mldb-backup-${var.env}-group"
}

resource "aws_iam_group_membership" "mldb-backup-env-group-membership" {
  name = "mldb-backup-${var.env}-group-membership"

  users = [
    "${aws_iam_user.mldb-backup-env-user.name}"
  ]

  group = "${aws_iam_group.mldb-backup-env-group.name}"
}

resource "aws_iam_group_policy_attachment" "mldb-backup-env-policy-attachment" {
  group      = "${aws_iam_group.mldb-backup-env-group.name}"
  policy_arn = "${aws_iam_policy.mldb-backup-env-storage-policy.arn}"
}

resource "aws_iam_policy" "mldb-backup-env-storage-policy" {
  name = "mldb-backup-${var.env}-storage"
  path = "/"
  description = "S3 Bucket used by usmcmarklogic"

policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action": "s3:ListAllMyBuckets",
         "Resource":"arn:aws-us-gov:s3:::*"
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:AbortMultipartUpload",
            "s3:ListBucket",
            "s3:ListBucketMultipartUploads",
            "s3:ListMultipartUploadParts",
            "s3:GetBucketLocation",
            "s3:PutObject",
            "s3:GetObject",
            "s3:DeleteObject"
         ],
         "Resource":[
            "arn:aws-us-gov:s3:::mldb-backup-${var.env}",
            "arn:aws-us-gov:s3:::mldb-backup-${var.env}/*"
         ]
      }
   ]
}
EOF
}

resource "aws_iam_policy" "mldb-backup-env-readonly-policy" {
  name = "mldb-backup-${var.env}-readonly"
  path = "/"
  description = "S3 Readonly"

policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action": "s3:ListAllMyBuckets",
         "Resource":"arn:aws-us-gov:s3:::*"
      },
      {
         "Effect":"Allow",
         "Action":[
            "s3:AbortMultipartUpload",
            "s3:ListBucket",
            "s3:ListBucketMultipartUploads",
            "s3:ListMultipartUploadParts",
            "s3:GetBucketLocation",
            "s3:GetObject"
         ],
         "Resource":[
            "arn:aws-us-gov:s3:::mldb-backup-${var.env}",
            "arn:aws-us-gov:s3:::mldb-backup-${var.env}/*",
            "arn:aws-us-gov:s3:::nifi-generated-files-${var.env}",
            "arn:aws-us-gov:s3:::nifi-generated-files-${var.env}/*"
         ]
      }
   ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "mldb-backup-env-readonly-policy-attach" {
  role = "mso_read_only"
  policy_arn = "${aws_iam_policy.mldb-backup-env-readonly-policy.arn}"
}

##CloudWatch 


#
resource "aws_iam_role" "CloudWatchAgentServer-Role" {
  name        = "${local.eid}-${var.customer-name}-CloudWatchAgentServer-Role"
  description = "Allows EC2 instances to call AWS services on your behalf."

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role" "SSM-Cloudwatch-Role" {
  name        = "${local.eid}-${var.customer-name}-SSM-Cloudwatch-Role"
  description = "Allows EC2 instances to call AWS services on your behalf."

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "ec2-cloudwatchagent-profile" {
  name = "${local.eid}-${var.customer-name}-ec2-cloudwatch-profile"
  role = aws_iam_role.SSM-Cloudwatch-Role.name
}

resource "aws_iam_instance_profile" "SSM-Cloudwatch-profile" {
  name = "${local.eid}-${var.customer-name}-SSM-Cloudwatch-profile"
  role = aws_iam_role.CloudWatchAgentServer-Role.name
}



resource "aws_iam_policy" "CloudWatchAgentServer-Policy" {
  name        = "${local.eid}-${var.customer-name}-CloudWatchAgentServer-Policy"
  description = "Permissions required to use AmazonCloudWatchAgent on servers"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData",
                "ec2:DescribeVolumes",
                "ec2:DescribeTags",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
                "logs:DescribeLogGroups",
                "logs:PutRetentionPolicy"
                
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter"
            ],
            "Resource": "arn:aws-us-gov:ssm:*:*:parameter/AmazonCloudWatch-*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "CloudWatchAgentServer-policy-att" {
  role       = aws_iam_role.CloudWatchAgentServer-Role.name
  policy_arn = aws_iam_policy.CloudWatchAgentServer-Policy.arn
}

resource "aws_iam_policy" "CloudWatchAgentAdmin-Policy" {
  name        = "${local.eid}-${var.customer-name}-CloudWatchAgentAdmin-Policy"
  description = "Full permissions required to use AmazonCloudWatchAgent."


  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "cloudwatch:PutMetricData",
                "ec2:DescribeTags",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams",
                "logs:DescribeLogGroups",
                "logs:PutRetentionPolicy"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssm:GetParameter",
                "ssm:PutParameter"
            ],
            "Resource": "arn:aws-us-gov:ssm:*:*:parameter/AmazonCloudWatch-*"
        }
    ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "CloudWatchAgentAdmin-policy-att" {
  role       = aws_iam_role.CloudWatchAgentServer-Role.name
  policy_arn = aws_iam_policy.CloudWatchAgentAdmin-Policy.arn
}

resource "aws_iam_policy" "AmazonSSMManagedInstanceCore-Policy" {
  name        = "${local.eid}-${var.customer-name}-AmazonSSMManagedInstanceCore-Policy"
  description = "Permissions required for AWS SSM to manage EC2 Instances"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:DescribeDocument",
                "ssm:GetManifest",
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:PutComplianceItems",
                "ssm:PutConfigurePackageResult",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssmmessages:CreateControlChannel",
                "ssmmessages:CreateDataChannel",
                "ssmmessages:OpenControlChannel",
                "ssmmessages:OpenDataChannel"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2messages:AcknowledgeMessage",
                "ec2messages:DeleteMessage",
                "ec2messages:FailMessage",
                "ec2messages:GetEndpoint",
                "ec2messages:GetMessages",
                "ec2messages:SendReply"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "SSM-CloudWatchAgentServer-policy-att" {
  role       = aws_iam_role.SSM-Cloudwatch-Role.name
  policy_arn = aws_iam_policy.CloudWatchAgentServer-Policy.arn
}

resource "aws_iam_role_policy_attachment" "AmazonSSMManagedInstanceCore-att" {
  role       = aws_iam_role.SSM-Cloudwatch-Role.name
  policy_arn = aws_iam_policy.AmazonSSMManagedInstanceCore-Policy.arn
}


# Added 27 March 2023 for MCCLOUD-5469
resource "aws_iam_policy" "marklogic-backup-s3-read-write-policy" {
  name = "marklogic-backup-s3-${var.env}-readwrite"
  path = "/"
  description = "marklogic Backup S3 Read-write"

policy = <<EOF
{
   "Version":"2012-10-17",
   "Statement":[
      {
         "Effect":"Allow",
         "Action": "s3:ListAllMyBuckets",
         "Resource":"arn:aws-us-gov:s3:::*"
      },
      {
         "Effect":"Allow",
         "Action": "s3:*Object",
         "Resource":[
           "arn:aws-us-gov:s3:::mldb-backup-prod",
           "arn:aws-us-gov:s3:::mldb-backup-prod/*",
           "arn:aws-us-gov:s3:::nifi-generated-files-prod",
           "arn:aws-us-gov:s3:::nifi-generated-files-prod/*",
           "arn:aws-us-gov:s3:::mldb-backup-staging",
           "arn:aws-us-gov:s3:::mldb-backup-staging/*",
           "arn:aws-us-gov:s3:::nifi-generated-files-staging",
           "arn:aws-us-gov:s3:::nifi-generated-files-staging/*"
         ]
      }
   ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "marklogic-backup-s3-read-write-policy_attach_role" {
  role       = "${aws_iam_role.SSM-Cloudwatch-Role.name}"
  policy_arn = "${aws_iam_policy.marklogic-backup-s3-read-write-policy.arn}"
}
